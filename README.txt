Overview
--------

This module uses cron to build a flat text file based on the users in a given role.  The
original intent was to build a DNS file with one line per user in the role.  However, 
additional features of low complexity have been added.

Features
-------

1) Optionally define a multi-line body of text for the file header.
2) Optionally define a multi-line body of text that is applied for each user in the group.
3) Optionally define a multi-line body of text for the file footer.
4) Use variables in each body of text, including: site name, number of users in the role,
   role name, role ID, current timestamp, user name (only in #2 above), and user ID (only
   in #2 above).
5) Optionally replace spaces in the site name, role name, and user name with a specified 
   string.
6) Optionally convert the site name, role name, and user name to lowercase.
7) Specify the minimum hours between cron updates.

Author
------

Nic Ivy (http://njivy.org)

README.txt version
------------------
